using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public Text scoreText;
    public Text coinsBronzeText;
    public Text coinsSilverText;
    public Text coinsGoldText;
    
    private int _score = 0;
    private int _coinsB = 0;
    private int _coinsS = 0;
    private int _coinsG = 0;

    private void Start()
    {
        scoreText.text = "Score: " + _score;
        PrintCoins();
    }


    public int GetScore()
    {
        return _score;
    }
    public void PrintCoins(){
        coinsBronzeText.text = "Coins: " + _coinsB;
        coinsSilverText.text = "Coins: " + _coinsS;
        coinsGoldText.text = "Coins: " + _coinsG;
    }
    public void PlusScore()
    {
        _score += 10;
        scoreText.text = "Score: " + _score;
    }
    public void PlusCoinsB(){
        _coinsB +=1;
        coinsBronzeText.text = "Coins: " + _coinsB;
    }
    public void PlusCoinsS(){
        _coinsS +=1;
        coinsSilverText.text = "Coins: " + _coinsS;
    }
    public void PlusCoinsG(){
        _coinsG +=1;
        coinsGoldText.text = "Coins: " + _coinsG;
    }
    /*
    public int GetCoints()
    {
        return _coins;
    }*/
}
